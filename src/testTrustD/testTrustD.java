package testTrustD;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
class testTrustD {

// MARK: Test Cases for TrustD.space.
	
// MARK: TEST CASES FOR THE LOGIN PAGE.
	
	// MARK: Open login page and enter values into text fields and clicking on signup button.
	
	@Test
		void testEnterValues() {
		System.setProperty("webdriver.chrome.driver","/Users/goney/desktop/chromedriver");
		WebDriver driver = new ChromeDriver();
		String baseUrl = "https://devmom.trustd.space";
		driver.get(baseUrl);
		WebElement firstName = driver.findElement(By.id("firstName"));
		WebElement lastName  = driver.findElement(By.id("lastName"));
		WebElement email = driver.findElement(By.id("email"));
		WebElement password = driver.findElement(By.id("pwd"));
		firstName.sendKeys("John");
		lastName.sendKeys("Rick");
		email.sendKeys("iboost@gmail.com");
		password.sendKeys("123456");
		WebElement signUp = driver.findElement(By.xpath("/html/body/app-root/app-shell/mat-sidenav-container/mat-sidenav-content/main/app-registration/div/div/div/div/form/div[5]/button"));
		signUp.click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.close();
}
	
	// MARK: TESTING GOOGLE SIGNUP BUTTON.
	
	@Test 
	void testgooglesignupButton() {
		System.setProperty("webdriver.chrome.driver","/Users/goney/desktop/chromedriver");
		WebDriver driver = new ChromeDriver();
		String baseUrl = "https://devmom.trustd.space";
		driver.get(baseUrl);
		WebElement googleSignUpButton = driver.findElement(By.xpath("/html/body/app-root/app-shell/mat-sidenav-container/mat-sidenav-content/main/app-registration/div/div/div/div/app-social-signup/div/a/button"));
		googleSignUpButton.click();
		try {
			Thread.sleep(5000);
} 
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
}
		driver.close();	
}
	// MARK: TESTING ALREADY HAVE AN ACCOUNT BUTTON.
	
	@Test 
	void testAlreadyHaveAnAccount() 
	{
		System.setProperty("webdriver.chrome.driver","/Users/goney/desktop/chromedriver");
		WebDriver driver = new ChromeDriver();
		String baseUrl = "https://devmom.trustd.space";
		driver.get(baseUrl);
		WebElement haveAnAccount = driver.findElement(By.xpath("/html/body/app-root/app-shell/mat-sidenav-container/mat-sidenav-content/main/app-registration/div/div/div/div/div[2]/div/a"));
		haveAnAccount.click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.close();	
		}
	
	
	
	
	
}







